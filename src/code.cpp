#include "code.hpp"
#include "parsing_utilities.hpp"

namespace nbbl {

code::code(const std::string& c)
    : m_pre_loop_part{}
    , m_post_loop_part{}
    , m_loop{}
{
    auto p = parsing_utilities::find_loop(c);
    m_pre_loop_part = c.substr(0, p.first);
    if (p.second != c.size() - 1) {
        m_post_loop_part = c.substr(p.second + 1);
    }
    m_loop = node::parse(c.substr(p.first, p.second - p.first + 1));
}

}
