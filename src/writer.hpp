#pragma once

#include <ostream>

namespace nbbl {

class code;
class node;

class writer
{
public:
    writer() = default;
    ~writer() = default;

public:
    std::string get_code(const code& c);
    std::string get_code(const node& c, const std::string& i = "");
    void write(const code& c, std::ostream& o);
    void write(const node& c, std::ostream& o);
};

}
