#include "node.hpp"
#include "parser.hpp"
#include "utility.hpp"

#include <sstream>

namespace nbbl {

// Parse function.
node::ptr node::parse(std::string body)
{
    if (body.empty()) {
        return ptr{};
    }
    parser p{std::move(body)};
    p.parse();
    auto c = parse(p.get_child_body());
    header h{p.variable(), p.iterate_from(), p.iterate_to(), p.header()};
    return create(std::move(h), p.get_start_operation(),
            std::move(c), p.get_end_operation());
}

node::ptr node::create(header h, std::string s, ptr c, std::string e)
{
    return node::ptr{new node{std::move(h), std::move(s), std::move(c), std::move(e)}};
}

node::node(header h, std::string s, ptr c, std::string e)
    : m_header{std::move(h)}
    , m_start_operation{std::move(s)}
    , m_end_operation{std::move(e)}
    , m_child_loop{std::move(c)}
{
}

node::node(const node& n)
    : m_header{n.m_header}
    , m_start_operation{n.m_start_operation}
    , m_end_operation{n.m_end_operation}
    , m_child_loop{new node{*(n.m_child_loop)}}
{
}

void node::make_perfect()
{
    if (!has_loop()) {
        return;
    }
    const auto& h = m_child_loop->get_header();
    if (!get_start_operation().empty()) {
        std::string indent = utility::get_indent(m_child_loop->get_start_operation());
        std::string s{"\n"}
        ;
        s += (indent + "if (" + h.variable() + " == " + h.iterate_from() + ") {\n");
        std::stringstream ss{get_start_operation()};
        std::string line{};
        while (std::getline(ss, line)) {
            utility::strip_spaces(line);
            s += indent + "    " + line + "\n";
        }
        s += (std::string{"\n"} + indent + "}\n" + m_child_loop->get_start_operation());
        m_child_loop->set_start_operation(std::move(s));
        set_start_operation("");
    }
    if (!get_end_operation().empty()) {
        std::string indent = utility::get_indent(m_child_loop->get_start_operation());
        std::string s{m_child_loop->get_end_operation()};
        s += (std::string{"\n"} + indent + "if (" + h.variable() + " == " + h.iterate_to() + ") {\n");
        std::stringstream ss{get_end_operation()};
        std::string line{};
        while (std::getline(ss, line)) {
            utility::strip_spaces(line);
            s += indent + "    " + line + "\n";
        }
        s += (std::string{"\n"} + indent + "}\n");
        m_child_loop->set_end_operation(std::move(s));
        set_end_operation("");
    }
    m_child_loop->make_perfect();
}

}
