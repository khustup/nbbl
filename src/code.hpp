#pragma once

#include "exceptions.hpp"
#include "node.hpp"

#include <string>

namespace nbbl {

class code
{
public:
    code(const std::string& c);

public:
    const std::string& get_pre_loop_part() const
    {
        return m_pre_loop_part;
    }

    const node& get_loop() const
    {
        if (!has_loop()) {
            throw has_not_loop{};
        }
        return *m_loop;
    }

    const std::string& get_post_loop_part() const
    {
        return m_post_loop_part;
    }

    bool has_loop() const
    {
        return static_cast<bool>(m_loop);
    }

public:
    void make_perfect()
    {
        if (has_loop()) {
            m_loop->make_perfect();
        }
    }

private:
    std::string m_pre_loop_part;
    std::string m_post_loop_part;
    node::ptr m_loop;
};

}
