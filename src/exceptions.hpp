#pragma once

#include <string>

namespace nbbl {

class exception
{
public:
    exception(std::string w)
        : m_what{std::move(w)}
    {
    }

    exception(const exception&) = default;
    exception& operator=(const exception&) = default;
    exception(exception&&) = default;
    exception& operator=(exception&&) = default;

public:
    const std::string& what() const
    {
        return m_what;
    }

private:
    std::string m_what;
};

class has_not_loop : public exception
{
public:
    has_not_loop()
        : exception{"Final node. Don't have child loops."}
    {
    }
};

}
