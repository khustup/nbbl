#include "code.hpp"
#include "writer.hpp"

#include <iostream>
#include <sstream>

std::string read_input() {
    std::stringstream buffer;
    std::string all_input;
    buffer << std::cin.rdbuf();
    all_input = buffer.str();
    return all_input;
}

int main(int, char**)
{
    std::string input = read_input();
    nbbl::code c{input};
    c.make_perfect();
    nbbl::writer w{};
    w.write(c, std::cout);
    return 0;
}
