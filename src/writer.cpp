#include "writer.hpp"
#include "code.hpp"
#include "node.hpp"
#include "utility.hpp"

#include <sstream>

namespace nbbl {

std::string writer::get_code(const code& c)
{
    std::string l{};
    if (c.has_loop()) {
        l = get_code(c.get_loop());
    }
    return c.get_pre_loop_part() + l + c.get_post_loop_part();
}

std::string writer::get_code(const node& n, const std::string& i)
{
    std::string l{};
    if (n.has_loop()) {
        auto ii = utility::get_indent(n.get_child_loop().get_start_operation());
        l = get_code(n.get_child_loop(), i + ii);
    }
    std::string r{};
    std::stringstream ss{std::string{"for "} + n.get_header().text() + " {\n" + n.get_start_operation() + l + n.get_end_operation() + "\n}"};
    while (std::getline(ss, l)) {
        r += (i + l + '\n');
    }
    return r;
}

void writer::write(const code& c, std::ostream& o)
{
    o << get_code(c);
}

void writer::write(const node& n, std::ostream& o)
{
    o << get_code(n);
}

}
