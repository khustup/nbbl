#pragma once

#include <string>
#include <tuple>

namespace nbbl {

namespace parsing_utilities {

using interval = std::pair<std::size_t, std::size_t>;
using header_data = std::tuple<std::string, std::string, std::string>;

interval find_loop(const std::string&);
interval find_header(const std::string&, std::size_t);
interval find_body(const std::string&, std::size_t);
header_data get_header_data(const std::string&);

}

}
