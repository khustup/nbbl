#pragma once

#include <string>

namespace nbbl {

class parser
{
public:
    parser(std::string body);
    ~parser() = default;
    parser(const parser&) = delete;
    parser& operator=(const parser&) = delete;
    parser(parser&&) = delete;
    parser& operator=(parser&&) = delete;

public:
    void parse();

public:
    /// @brief Returns header of loop (init, condition and loop expression).
    const std::string& variable() const
    {
        return m_variable;
    }

    const std::string& iterate_from() const
    {
        return m_interval_start;
    }

    const std::string& iterate_to() const
    {
        return m_interval_end;
    }

    const std::string& header() const
    {
        return m_header;
    }
    
    /// @brief Returns start operation of loop body.
    const std::string& get_start_operation() const
    {
        return m_start_operation;
    }

    /// @brief Returns end operation of loop body.
    const std::string& get_end_operation() const
    {
        return m_end_operation;
    }

    /// @brief Returns the child loop body.
    const std::string& get_child_body() const
    {
        return m_child_body;
    }

private:
    void parse_header(const std::string& h);

private:
    std::string m_body;
    std::string m_start_operation;
    std::string m_end_operation;
    std::string m_child_body;
    std::string m_variable;
    std::string m_interval_start;
    std::string m_interval_end;
    std::string m_header;

};

}
