#include "parsing_utilities.hpp"
#include "utility.hpp"

#include <algorithm>

namespace nbbl {

namespace parsing_utilities {

namespace {

class const_code_iterator
{
public:
    const_code_iterator(const std::string& c)
        : m_code{c}
        , m_pos{0}
    {}

    const_code_iterator(const const_code_iterator&) = default;
    const_code_iterator(const_code_iterator&&) = default;

public:
    const char& operator*() const
    {
        return m_code[m_pos];
    }

    const char& operator->() const
    {
        return m_code[m_pos];
    }

    const_code_iterator& operator++();

    const_code_iterator operator++(int)
    {
        auto t = *this;
        ++m_pos;
        return t;
    }

    operator std::size_t() const
    {
        return m_pos;
    }

    bool operator==(const_code_iterator i) const
    {
        return m_pos == i.m_pos && m_code == i.m_code;
    }

    bool operator!=(const_code_iterator i) const
    {
        return !((*this) == i);
    }

public:
    static const_code_iterator begin(const std::string& c)
    {
        const_code_iterator i{c};
        return i;
    }

    static const_code_iterator end(const std::string& c)
    {
        const_code_iterator i{c};
        i.m_pos = c.size();
        return i;
    }

    static const_code_iterator iter(const std::string& c, std::size_t p)
    {
        const_code_iterator i{c};
        i.m_pos = p;
        return i;
    }

private:
    void go_quote_end();
    void go_comment_end();
    void go_multiline_comment_end();

private:
    std::string m_code;
    std::size_t m_pos;
};

unsigned backslashes_count(const std::string& s, std::size_t i)
{
    if (i == std::string::npos) {
        return 0;
    }
    unsigned c{0};
    while (s[--i] == '\\') {
        ++c;
    }
    return c;
}

void const_code_iterator::go_quote_end()
{
    while (m_pos != std::string::npos) {
        ++m_pos;
        m_pos = m_code.find('"', m_pos);
        auto c = backslashes_count(m_code, m_pos);
        if (m_pos != std::string::npos && c % 2 == 0) {
            break;
        }
    }
}

void const_code_iterator::go_comment_end()
{
    m_pos = m_code.find('\n', m_pos);
}

void const_code_iterator::go_multiline_comment_end()
{
    while (m_pos != std::string::npos) {
        ++m_pos;
        m_pos = m_code.find('/', m_pos);
        if (m_pos != std::string::npos && m_code[m_pos - 1] == '*') {
            break;
        }
    }
}

const_code_iterator& const_code_iterator::operator++()
{
    if (m_pos == std::string::npos) {
        return *this;
    }
    if (m_code[m_pos] == '"') {
        go_quote_end();
    } else if (m_code[m_pos] == '/' && m_code[m_pos + 1] == '/') {
        go_comment_end();
    } else if (m_code[m_pos] == '/' && m_code[m_pos + 1] == '*') {
        go_multiline_comment_end();
    }
    if (m_pos != std::string::npos) {
        ++m_pos;
    }
    return *this;
}

bool equals(const_code_iterator i, const std::string& l)
{
    for (auto c : l) {
        if (*i++ != c) {
            return false;
        }
    }
    return true;
}

bool is_breaker(char c)
{
    return utility::is_separator(c) || c == ';';
}

bool is_alphanumeric(char c)
{
    return c == '_' || (c >= 'a' && c <= 'z') ||
        (c >= 'A' && c <= 'A') ||
        (c >= '0' && c <= '9');
}

std::size_t find_keyword(const std::string& c, const std::string& l)
{
    const_code_iterator i{c};
    while (i != const_code_iterator::end(c)) {
        auto p = static_cast<std::size_t>(i);
        if (c.size() - p >= l.size() && equals(i, l) &&
                (p == 0 || is_breaker(c[p - 1])) && 
                !is_alphanumeric(c[p + l.size()])) {
            return i;
        }
        ++i;
    }
    return std::string::npos;
}

std::size_t find_end_of_for(const std::string& c, std::size_t s)
{
    auto p = find_header(c, s);
    auto b = find_body(c, p.second);
    return b.second;
}

std::size_t find_closed_brace(const_code_iterator s, const_code_iterator e)
{
    unsigned x{0};
    while (s != e) {
        if (*s == '{') {
            ++x;
        } else if (*s == '}') {
            --x;
            if (x == 0) {
                return s;
            }
        }
        ++s;
    }
    return std::string::npos;
}

std::size_t find_in_code(const_code_iterator s, const_code_iterator e,
        char c)
{
    while (s != e) {
        if (*s == c) {
            return s;
        }
        ++s;
    }
    return std::string::npos;
}

void parse_init(const std::string& c, header_data& d)
{
    const_code_iterator i(c);
    auto x = find_in_code(i, const_code_iterator::end(c), '=');
    if (x != std::string::npos) {
        std::string v{c.substr(0, x)};
        utility::strip_spaces(v);
        std::get<0>(d) = std::move(v);
        v = c.substr(x + 1);
        auto cc = find_in_code(const_code_iterator(v), const_code_iterator::end(v), ',');
        if (cc != std::string::npos) {
            v = v.substr(0, cc);
        }
        utility::strip_spaces(v);
        std::get<1>(d) = std::move(v);
    }
}

void parse_condition(const std::string& c, header_data& d)
{
    const_code_iterator i(c);
    auto x = find_in_code(i, const_code_iterator::end(c), '<');
    if (x != std::string::npos && x != c.size() - 1) {
        std::string u{};
        if (c[x + 1] == '=') {
            u = c.substr(x + 2);
            utility::strip_spaces(u);
        } else {
            u = c.substr(x + 1);
            utility::strip_spaces(u);
            u += " - 1";
            utility::strip_spaces(u);
        }
        std::get<2>(d) = std::move(u);
    }
}

}

interval find_loop(const std::string& c)
{
    std::size_t s{find_keyword(c, "for")};
    std::size_t e{find_end_of_for(c, s)};
    return std::make_pair(s, e);
}

interval find_header(const std::string& c, std::size_t s)
{
    const_code_iterator i{const_code_iterator::iter(c, s + 3)};
    interval rr{std::string::npos, std::string::npos};
    unsigned x{0};
    while (i != const_code_iterator::end(c)) {
        if (*i == '(') {
            if (x == 0) {
                rr.first = i;
            }
            ++x;
        } else if (*i == ')') {
            --x;
            if (x == 0) {
                rr.second = i;
                break;
            }
        }
        if (x == 0 && !utility::is_separator(*i)) {
            return rr;
        }
        ++i;
                
    }
    if (x != 0) {
        rr.first = std::string::npos;
    }
    return rr;
}

interval find_body(const std::string& c, std::size_t s)
{
    const_code_iterator i{const_code_iterator::iter(c, s + 1)};
    interval rr{std::string::npos, std::string::npos};
    while (i != const_code_iterator::end(c)) {
        if (!utility::is_separator(*i)) {
            rr.first = i;
            if (*i == '{') {
                rr.second = find_closed_brace(i, const_code_iterator(c));
            } else {
                rr.second = find_in_code(i, const_code_iterator::end(c), ';');
            }
            break;
        }
        ++i;
    }
    return rr;
}

header_data get_header_data(const std::string& c)
{
    header_data d{};
    const_code_iterator i{const_code_iterator::iter(c, 1)};
    auto j = find_in_code(i, const_code_iterator::end(c), ';');
    if (j == std::string::npos) {
        return d;
    }
    auto init = c.substr(1, j - 1);
    const_code_iterator i1 = const_code_iterator::iter(c, j + 1);
    auto k = find_in_code(i1, const_code_iterator::end(c), ';');
    if (k == std::string::npos) {
        return d;
    }
    auto cond = c.substr(j + 1, k - j - 1);
    utility::strip_spaces(init);
    utility::strip_spaces(cond);
    parse_init(init, d);
    parse_condition(cond, d);
    return d;
}

}

}
