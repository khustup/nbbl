#pragma once

#include "exceptions.hpp"

#include <memory>
#include <string>

namespace nbbl {

/**
 * @class header.
 * @brief Header of loop.
 *
 * Contains the information about loop header:
 * - iteration variable,
 * - iteration interval,
 * - iteration step.
 */
class header
{
public:
    header(std::string v, std::string f, std::string t, std::string s)
        : m_variable{std::move(v)}
        , m_interval_start{std::move(f)}
        , m_interval_end{std::move(t)}
        , m_text{std::move(s)}
    {
    }

    header(const header&) = default;
    header(header&&) = default;
    ~header() = default;

public:
    const std::string& variable() const
    {
        return m_variable;
    }

    const std::string& iterate_from() const
    {
        return m_interval_start;
    }

    const std::string& iterate_to() const
    {
        return m_interval_end;
    }

    const std::string& text() const
    {
        return m_text;
    }

private:
    std::string m_variable;
    std::string m_interval_start;
    std::string m_interval_end;
    std::string m_text;
};

/**
 * @class node.
 * @brief Represents one node of for loop.
 *
 * Node is single loop body, which can contain child loop.
 * Each loop node has the following properties:
 * - Iteration interval.
 * - Start operation, based on iteration variable.
 * - Child loop node.
 * - End operation.
 */
class node
{
public:
    using ptr = std::unique_ptr<node>;

    /**
     * @brief Creates the node based on given loop body.
     * @param body Body of loop.
     * @return Created node
     */
    static ptr parse(std::string body);

    /**
     * @brief Creates the node by given properties and returns it.
     * @param h Header of loop.
     * @param s Start operation of loop.
     * @param c Child loop.
     * @param e End operation of loop.
     * @return Created node.
     */
    static ptr create(header h, std::string s, ptr c, std::string e);

public:
    void make_perfect();

public:
    /// @brief Returns header of loop (init, condition and loop expression).
    const header& get_header() const
    {
        return m_header;
    }

    /// @brief Returns start operation of loop body.
    const std::string get_start_operation() const
    {
        return m_start_operation;
    }

    /// @brief Sets the start operation of loop.
    void set_start_operation(std::string s)
    {
        m_start_operation = std::move(s);
    }

    /// @brief Returns end operation of loop body.
    const std::string get_end_operation() const
    {
        return m_end_operation;
    }

    /// @brief Sets the end operation of loop.
    void set_end_operation(std::string e)
    {
        m_end_operation = std::move(e);
    }

    /// @brief Check whether loop is final, i.e. hasn't child loop.
    bool has_loop() const
    {
        return static_cast<bool>(m_child_loop);
    }

    /// @brief Returns child loop.
    const node& get_child_loop() const
    {
        if (!has_loop()) {
            throw has_not_loop{};
        }
        return *m_child_loop;
    }

public:
    node(header h, std::string s, ptr c, std::string e);
    node(const node&);
    node(node&&) = default;
    node& operator =(const node&) = delete;
    node& operator =(node&&) = delete;
    ~node() = default;

private:
    header m_header;
    std::string m_start_operation;
    std::string m_end_operation;
    ptr m_child_loop;
};

}
