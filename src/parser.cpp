#include "parser.hpp"
#include "parsing_utilities.hpp"
#include "utility.hpp"

namespace nbbl {

parser::parser(std::string body)
    : m_body{std::move(body)}
    , m_start_operation{}
    , m_end_operation{}
    , m_child_body{}
    , m_variable{}
    , m_interval_start{}
    , m_interval_end{}
    , m_header{}
{
}

namespace {

bool contains_logic(const std::string& n)
{
    std::string::const_iterator i{n.begin()};
    while (i != n.end()) {
        if (!utility::is_separator(*i)) {
            return true;
        }
        ++i;
    }
    return false;
}

void correct_begin(std::string& n)
{
    while (!n.empty() && utility::is_separator(n[0])) {
        n.erase(0, 1);
    }
    if (n.empty()) {
        return;
    }
    if (n[0] == '{') {
        n.erase(0, 1);
    }
    if (contains_logic(n)) {
        while (!n.empty() && utility::is_separator(n[0])) {
            auto c = n[0];
            n.erase(0, 1);
            if (c == '\n') {
                break;
            }
        }
    } else {
        n = "";
    }
}

void correct_end(std::string& n)
{
    while (!n.empty() && utility::is_separator(n[n.size() - 1])) {
        n.erase(n.size() - 1, 1);
    }
    if (n.empty()) {
        return;
    }
    if (n[n.size() - 1] == '}') {
        n.erase(n.size() - 1, 1);
    }
    if (contains_logic(n)) {
        while (!n.empty() && utility::is_separator(n[n.size() - 1])) {
            auto c = n[n.size() - 1];
            n.erase(n.size() - 1, 1);
            if (c == '\n') {
                break;
            }
        }
    } else {
        n = "";
    }
}

}

void parser::parse()
{
    auto h = parsing_utilities::find_header(m_body, 0);
    auto b = parsing_utilities::find_body(m_body, h.second);
    m_header = m_body.substr(h.first, h.second - h.first + 1);
    parse_header(m_header);
    std::string body = m_body.substr(b.first, b.second - b.first + 1);
    auto child_loop = parsing_utilities::find_loop(body);
    if (child_loop.first != std::string::npos) {
        m_child_body = body.substr(child_loop.first, 
                child_loop.second - child_loop.first + 1);
        m_start_operation = body.substr(0, child_loop.first - 1);
        m_end_operation = body.substr(child_loop.second + 1);
    } else {
        m_start_operation = std::move(body);
    }
    correct_begin(m_start_operation);
    if (m_end_operation.empty()) {
        correct_end(m_start_operation);
    } else {
        correct_end(m_end_operation);
    }
}

void parser::parse_header(const std::string& c)
{
    auto x = parsing_utilities::get_header_data(c);
    m_variable = std::get<0>(x);
    m_interval_start = std::get<1>(x);
    m_interval_end = std::get<2>(x);
}

}
