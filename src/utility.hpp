#pragma once

namespace nbbl {

namespace utility {

inline bool is_separator(char c) 
{
    return c == ' ' || c == '\t' || c == '\n';
}

inline void strip_spaces(std::string& c)
{
    std::size_t x = 0;
    while (!c.empty() && utility::is_separator(c[x])) {
        c.erase(x, 1);
    }
    x = c.size() - 1;
    while (!c.empty() && utility::is_separator(c[x])) {
        c.erase(x, 1);
        x = c.size() - 1;
    }
}

inline std::string get_indent(const std::string& c)
{
    auto x = c.find_first_not_of(" \t");
    if (x == 0) {
        return "    ";
    }
    return c.substr(0, x);
}

}

}
